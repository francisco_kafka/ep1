#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente {

	private:
		string Nome_cliente;
		int Cliente_socio; // 1- sim ,0 - não
		string Historico_de_compras;
	public:
		Cliente();

	string get_Nome_cliente();
	void set_Nome_cliente(string nome_cliente);
	int get_Cliente_socio();
	void set_Cliente_socio(int Cliente_socio);
	string Historico_de_compras();
	void Historico_de_compras(string Historico_de_compras);

};

#endif
