#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <iostream>
using namespace std;

class Produto {

	private:

		string Nome;
		int Quantidade;

	public:
	//metodos
	 Produto(); //Metodo construtor
	 ~Produto();//Metodo destrutor

	//Metodos Acessores
	string get_Nome();
	void set_Nome(string Nome);
	int get_Quantidade();
	void set_Quantidade(int Quantidade);

};

#endif
